# An AngularJS service wrapping underscore

An angular underscore service providing the entire underscore API.

Angular filters for most underscore functions are provided, but not recommended to use due to have processing.


## Filters Provided

Note: Filters are a wip. You may receive an infinite digest loop error. It is a better practice to utilize underscore
in a controller or service, rather than in html. Hence, filters are provided as a separate file.

* map
* collect
* reduce
* inject
* foldl
* reduceRight
* foldr
* find
* detect
* filter
* select
* where
* findWhere
* reject
* invoke
* pluck
* max
* min
* sortBy
* groupBy
* indexBy
* countBy
* shuffle
* sample
* toArray
* size
* first 
* head 
* take
* initial
* last
* rest 
* tail
* drop
* compact
* flatten
* without
* partition
* union
* intersection
* difference
* uniq 
* unique
* zip
* object
* indexOf
* lastIndexOf
* sortedIndex
* keys
* values
* pairs
* invert
* functions 
* methods
* pick
* omit
* tap
* identity
* uniqueId
* escape
* unescape
* result
* template


For API details please check out [underscorejs.org](http://underscorejs.org/)

## How to use

### Install

NPM:

```bash

bower install pb-angular-underscore
```

After load angular.js and underscore.js:

```html
<script type="text/javascript" src="bower_components/pb-angular-underscore.js"></script>
```

If you want to load the filters:

```html
<script type="text/javascript" src="bower_components/pb-angular-underscore-filters.js"></script>
```

### Load angular-underscore

#### Provide Underscore as a dependency to your app

```javascript
angular.module('myApp', ['pb.angular.underscore']);
```

#### Provide filters as a dependency

```javascript
angular.module('myApp', ['pb.angular.underscore']);

```

If you want to load the filters:

```javascript
angular.module('myApp', ['pb.angular.underscore', 'pb.angular.underscore.filters']);
```


### Usecase

#### From the Template

```html
<script type="text/javascript">
  angular.module('example', ['pb.angular.underscore', 'pb.angular.underscore.filters']);
</script>

<body ng-app="example">
  <!-- generate 10 unduplicated random number from 0 to 9 -->
  <div ng-repeat="num in range(10)|shuffle">{{num}}</div>
</body>
```

#### From the Controller, Service, or Directive

```javascript
angular.module('yourAwesomeApp', ['pb.angular.underscore'])
.controller('yourAwesomeCtrl', ['$scope', 'underscore', function($scope, _) {
    $scope.sample = _.sample([1, 2, 3]); // got 1, or 2, or 3.
});

```

### Local build

```
$ npm install
$ grunt package
```

## License

(The MIT License)

Copyright (c) 2014 <jerryorta-dev@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.