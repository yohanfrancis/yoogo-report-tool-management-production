/**
 * @license Project Builder Angular Underscore v1.8.4-local+
 * (c) 2010-2015 Jerry Orta https://github.com/project-builder/pb-angular-underscore
 * License: MIT
 */
(function(window, angular, undefined) {'use strict';

angular.module('pb.angular.underscore.filters', ['ng', 'pb.angular.underscore']);

var adapList = [
    ['map', 'collect'],
    ['reduce', 'inject', 'foldl'],
    ['reduceRight', 'foldr'],
    ['find', 'detect'],
    ['filter', 'select'],
    'where',
    'findWhere',
    'reject',
    'invoke',
    'pluck',
    'max',
    'min',
    'sortBy',
    'groupBy',
    'indexBy',
    'countBy',
    'shuffle',
    'sample',
    'toArray',
    'size',
    ['first', 'head', 'take'],
    'initial',
    'last',
    ['rest', 'tail', 'drop'],
    'compact',
    'flatten',
    'without',
    'partition',
    'union',
    'intersection',
    'difference',
    ['uniq', 'unique'],
    'zip',
    'object',
    'indexOf',
    'lastIndexOf',
    'sortedIndex',
    'keys',
    'values',
    'pairs',
    'invert',
    ['functions', 'methods'],
    'pick',
    'omit',
    'tap',
    'identity',
    'uniqueId',
    'escape',
    'unescape',
    'result',
    'template'
];

function isArray(obj) {

    if (Array.isArray) {
        return Array.isArray(obj);
    }

    return function (obj) {
        return toString.call(obj) === '[object Array]';
    };
}


var adapListLen = adapList.length;
for (var i = 0; i < adapListLen; i++) {
    var filterNames = adapList[i];

    if (!isArray(filterNames)) {
        filterNames = [filterNames];
    }

    for (var j = 0; j < filterNames.length; j++) {

        (function (ng, capturedFilterName) {
            angular.module('pb.angular.underscore.filters').filter(capturedFilterName, ['underscore', function (_) {
                //return _[capturedFilterName];
                return _.bind(_[capturedFilterName], _);
            }]);
        }(angular, filterNames[j]));
    }
}


})(window, window.angular);
