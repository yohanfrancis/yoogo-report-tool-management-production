'use strict';
$ = jQuery;
angular
  .module('yapp', [
    'service',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'snap',
    'ngAnimate',
    'gridster',
    'highcharts-ng',
    'pb.angular.underscore',
    'ui.sortable',
    'Alertify',
    'ngSanitize',
    'ngCsv'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/404');
    $stateProvider
      .state("auth", {
        abstract: true,
        url: "",
        templateUrl: 'layout/base.html',
        resolve: {}
      })
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'layout/base.html'
      })
      .state('404', {
        url: '/404',
        templateUrl: '404.html',
      })
      .state('company', {
        url: '/company/{cid}/{time}/{token}',
        templateUrl: 'views/dashboard/dashboard.html',
        controller: 'DashboardCtrl',
      })
      .state('dashboard', {
        url: '/dashboard/{id}',
        parent: 'auth',
        templateUrl: 'views/dashboard/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .state('dashboardIframe', {
        url: '/dashboard-iframe/{id}',
        parent: 'auth',
        templateUrl: 'views/dashboard/dashboard-iframe.html',
        controller: 'DashboardCtrl'
      })

      //Chart
      .state('newchart', {
        url: '/new-chart',
        parent: 'auth',
        templateUrl: 'views/chart/new_chart.html',
        controller: 'ChartCtrl'
      })
      .state('editchart', {
        url: '/edit-chart/{id}',
        parent: 'auth',
        templateUrl: 'views/chart/new_chart.html',
        controller: 'ChartCtrl'
      })
      .state('viewchart', {
        url: '/view-chart/{type}/{id}',
        parent: 'auth',
        templateUrl: 'views/chart/view_chart.html',
        controller: 'ViewChartCtrl'
      })

  });
