'use strict';
$ = jQuery;

angular.module('yapp')
  .filter('startFrom', function () {
    return function (input, start) {
      if (input) {
        if (isNaN(start))start = 0;
        start = +start; // Parse to int
        return input.slice(start);
      }
      return [];
    }
  })
  .filter('inArray', function () {
    return function (array, value) {
      return array.indexOf(value) !== -1;
    };
  })
  .filter('inArray', function () {
    return function (array, value) {
      return array.indexOf(value) !== -1;
    };
  })
  .filter('unit', function ($rootScope, $filter) {
    return function (input, unit, unit_option) {
      if (typeof unit == 'undefined' || unit == '' || typeof unit_option == 'undefined' || unit_option == '' || unit_option == 'none') {
        return input;
      }

      var unit_data = $rootScope.units[unit][unit_option];
      if (!unit_data) {
        return input;
      }

      if (unit == 'distance') {
        if (unit_option == 'm') {
          input = input * 1000;
        }
        if (unit_option == 'mile') {
          input = input / 1.609344;
        }
        if (input > 1) {
          input = $filter('number')(input);
          var suffix = unit_data.suffix;
          if (suffix == 'km') {
            var suffix = suffix + "'";
          }
          return unit_data.prefix + input + ' ' + suffix + 's';
        }
      }

      if (unit == 'last_time') {
        var date = new Date(input);
        var t = Math.floor(Math.abs(Date.now() - date) / 60000);
        return $rootScope.minute_to_natural_time(t);
      }

      if (unit == 'duration') {
        if (input == 0) return 0;
        return $rootScope.minute_to_natural_time(input);
      }

      if (unit == 'time') {
        if (unit_option == 'hour') {
          input = input / 60;
        }

        if (input > 1) {
          return unit_data.prefix + input + ' ' + unit_data.suffix + 's';
        }
      }

      if (unit == 'datetime') {
        if (typeof input == 'object') {
          var date = $filter('date')(input, unit_data.label);
          return date;
        } else if (typeof input == 'string') {
          var tmp_date = new Date(Date.parse(input.substr(0, 10) + "T" + input.substr(11, 8) + "Z"));
          var date = $filter('date')(tmp_date, unit_data.label);
          return date;
        } else {
          return input;
        }
      }

      input = $filter('number')(input);
      return unit_data.prefix + input + ' ' + unit_data.suffix;
    }
  })
  .filter('ceil', function () {
    return function (input) {
      return Math.ceil(input);
    };
  });
;
