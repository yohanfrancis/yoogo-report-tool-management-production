'use strict';
$ = jQuery;
var marker = null;
var map = null;
/**
 * @ngdoc function
 * @name yapp.controller:ChartCtrl
 * @description
 * # ChartCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('ChartCtrl', ['$scope', 'Auth', '$state', '$stateParams', '$rootScope', 'underscore', '$uibModal', 'reportAPI', 'Alertify', '$location', '$timeout',
    function ($scope, Auth, $state, $stateParams, $rootScope, _, $uibModal, reportAPI, Alertify, $location, $timeout) {
      //init data
      $rootScope.init_default_dashboad_id();
      $rootScope.clear_interval_reload();
      $rootScope.depths = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50];
      $scope.limit_items = [10, 20, 30, 40, 50, 100];
      $scope.filter_positions = ['top', 'bottom', 'left', 'right'];
      $scope.true_false = ['True', 'False'];

      reportAPI.get('models').success(function (resp) {
        $scope.select_models = resp;
        for (var i in $scope.select_models) {
          var temp = $scope.select_models[i]['fields'];
          var array_rebuild = [];
          for (var index in temp) {
            array_rebuild.push(temp[index]);
          }
          $scope.select_models[i]['fields'] = array_rebuild;
        }
      }).error(function (error) {
        try {
          Alertify.error(error.title + ': ' + error.description);
        } catch (e) {
          Alertify.error("Error occurred during connection to server");
        }
      });
      reportAPI.get('dashboards').success(function (resp) {
        $scope.dashboards = resp;
      }).error(function (error) {
        try {
          Alertify.error(error.title + ': ' + error.description);
        } catch (e) {
          Alertify.error("Error occurred during connection to server");
        }
      });

      $scope.model_change = function () {
        $scope.chart_settings.config.fields = [];
        $scope.chart_settings.config.filters = [];
        $scope.chart_settings.config.filter_data = [];
        $scope.chart_settings.config.dimx = [];
        $scope.chart_settings.config.dimy = [];
        $scope.chart_settings.config.fact = [];
      };

      $scope.edit_field = function (field_data, field_type) {
        $scope.animationsEnabled = true;
        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'views/popup/custom_field.html',
          controller: 'CustomFieldCtrl',
          size: 'lg',//sm
          resolve: {
            chart_type: function () {
              return $scope.chart_settings.type;
            },
            field_data: function () {
              return field_data;
            },
            field_type: function () {
              return field_type;
            },
            count_fact: function () {
              return $scope.chart_settings.config.fact.length;
            },
            model: function () {
              return $scope.chart_settings.model;
            }
          }
        });
        $rootScope.popups.push(modalInstance);
      };

      //init data
      if ($state.current.name == 'editchart') {
        reportAPI.get('chart/' + $stateParams.id)
          .success(function (resp) {
            if (_.isEmpty(resp)) {
              Alertify.error('This chart is not existed!');
              $location.path('/dashboard/' + $scope.chart_settings.dashboard_id);
            } else {
              $scope.chart_settings = resp;
            }
            $scope.is_preview = 1;
          }).error(function (error) {
            try {
              Alertify.error(error.title + ': ' + error.description);
            } catch (e) {
              Alertify.error("Error occurred during connection to server");
            }
          });
      } else {
        $scope.chart_settings = {
          name: '',
          model: 'bookgirl',
          type: 'table',
          option: 'none',
          limit: 10,
          depth: 0,
          filter_position: 'top',
          fulldim: 'False',
          inverted: 'False',
          reload: '0',
          dashboard_id: $rootScope.default_dashboard_id,
          config: {
            fields: [],
            filters: [],
            filter_data: [],
            dimx: [],
            dimy: [],
            fact: [],
          }
        };
      }
      //Drag, drop field
      $scope.draggableOptions = {
        connectWith: ".connected-drop-target-sortable",
        stop: function (e, ui) {
          ui.item.sortable.model = $rootScope.init_field_data(ui.item.sortable.model, $(ui.item.sortable.droptarget).attr('field_type'));
          // if the element is removed from the first container
          if (ui.item.sortable.source.hasClass('draggable-element-container') &&
            ui.item.sortable.droptarget &&
            ui.item.sortable.droptarget != ui.item.sortable.source &&
            ui.item.sortable.droptarget.hasClass('connected-drop-target-sortable')) {
            // restore the removed item
            ui.item.sortable.sourceModel.push(angular.copy(ui.item.sortable.model));
          }
          $scope.validate[$(ui.item.sortable.droptarget).attr('id')] = 0;

          var restrict = $(ui.item.sortable.droptarget).attr('restrict');
          if (restrict && ui.item.sortable.model[restrict] === false) {
            var index = _.indexOf(ui.item.sortable.droptargetModel, ui.item.sortable.model);
            ui.item.sortable.droptargetModel.splice(index, 1);
            Alertify.error("Can't drag field \"" + ui.item.sortable.model.label + "\" to this " + restrict + ".");
            return false;
          }

          var limit = parseInt($(ui.item.sortable.droptarget).attr('limit'));
          var error_mes = "Can't drag more field!";
          if ($rootScope.inArray($scope.chart_settings.type, ['line', 'column', 'bar'])) {
            if ($(ui.item.sortable.droptarget).attr('field_type') == 'fact' && $scope.chart_settings.config.dimx.length == 1 && $scope.chart_settings.config.dimy.length == 0) {
              limit = 100;
            }
            if ($(ui.item.sortable.droptarget).attr('field_type') == 'dimy' && $scope.chart_settings.config.dimx.length == 1 && $scope.chart_settings.config.fact.length > 1) {
              limit = 0;
              error_mes = "Can't drag more dim-y field! You have too many facts";
            }
          }

          if (ui.item.sortable.droptargetModel && ui.item.sortable.droptargetModel.length > limit) {
            var index = _.indexOf(ui.item.sortable.droptargetModel, ui.item.sortable.model);
            ui.item.sortable.droptargetModel.splice(index, 1);
            Alertify.error(error_mes);
            return false;
          }
        }
      };
      $scope.validate = {
        report_field_lists: 0,
        dimx_field_lists: 0,
        dimy_field_lists: 0,
        fact_field_lists: 0,
        dashboard_id: 0
      };
      $scope.remove_error = function (field) {
        $scope.validate[field] = 0;
      };

      $scope.save = function (is_preview) {
        $scope.validate = {
          report_field_lists: 0,
          dimx_field_lists: 0,
          dimy_field_lists: 0,
          fact_field_lists: 0,
          dashboard_id: 0
        };
        switch ($scope.chart_settings.type) {
          case 'table':
            if ($scope.chart_settings.config.fields.length == 0) {
              $scope.validate.report_field_lists = 1;
            }
            break;
          case 'column':
          case 'bar':
          case 'area':
          case 'group':
          case 'line':
            if ($scope.chart_settings.config.dimx.length == 0) {
              $scope.validate.dimx_field_lists = 1;
            }
            if ($scope.chart_settings.config.dimy.length == 0 && $scope.chart_settings.config.fact.length == 1) {
              $scope.validate.dimy_field_lists = 1;
            }
            if ($scope.chart_settings.config.fact.length == 0) {
              $scope.validate.fact_field_lists = 1;
            }
            break;
          case 'pie':
            if ($scope.chart_settings.config.dimx.length == 0) {
              $scope.validate.dimx_field_lists = 1;
            }
            if ($scope.chart_settings.config.fact.length == 0) {
              $scope.validate.fact_field_lists = 1;
            }
            break;
        }
        if (!$scope.chart_settings.dashboard_id) {
          $scope.validate.dashboard_id = 1;
        }
        if (!$("#new_chart").validationEngine('validate') || $scope.validate.dashboard_id == 1 || $scope.validate.report_field_lists == 1 || $scope.validate.dimx_field_lists == 1 || $scope.validate.dimy_field_lists == 1 || $scope.validate.fact_field_lists == 1) {
          Alertify.error("Failed to save chart. You need to correct all error field!");
          return false;
        }

        var post_put = $state.current.name == 'newchart' ? 'post' : 'put';
        var url = $state.current.name == 'newchart' ? 'chart' : 'chart/' + $stateParams.id;
        reportAPI[post_put](url, $scope.chart_settings).success(function (resp) {
          $scope.chart_settings = resp;
          if (typeof(Storage) !== "undefined") {
            sessionStorage.removeItem('report_' + resp.id.toString());
          }
          if (is_preview == 0) {
            $location.path('/dashboard/' + $scope.chart_settings.dashboard_id);
          } else {
            if (post_put == 'post') {
              $location.path('/edit-chart/' + $scope.chart_settings.id);
            } else {
              $scope.is_preview = Date.now();
            }

          }
        }).error(function (error) {
          try {
            Alertify.error(error.title + ': ' + error.description);
          } catch (e) {
            Alertify.error("Error occurred during connection to server");
          }
        });
      };
      $scope.delete = function () {
        if ($scope.chart_settings.id) {
          reportAPI.delete('chart/' + $scope.chart_settings.id)
            .success(function (resp) {
              $location.path('/dashboard/' + $scope.chart_settings.dashboard_id);
            }).error(function (error) {
              try {
                Alertify.error(error.title + ': ' + error.description);
              } catch (e) {
                Alertify.error("Error occurred during connection to server");
              }
            });
        }
      };
      //validate
      $rootScope.validation();
    }])
  .controller('CustomFieldCtrl', function ($scope, $rootScope, $uibModalInstance, reportAPI, chart_type, field_data, field_type, count_fact, model, $timeout) {

    if (field_type == 'filter_data' && field_data.filter_option == true) {
      $scope.field_type = field_type;
      var filter_value = field_data['filter_value'];
      delete field_data['filter_value'];
      $scope.field_data = field_data;

      reportAPI.get('filter/' + model + '/' + field_data.name)
        .success(function (resp) {
          $scope.filter_values = resp;

          $scope.field_data.filter_value = filter_value;
          $scope.field_data_cancel = angular.copy($scope.field_data);

        });
    } else {
      console.log('field_data', field_data);
      console.log('field_type', field_type);
      $scope.field_data = field_data;
      $scope.field_type = field_type;
      $scope.field_data_cancel = angular.copy($scope.field_data);
    }

    $scope.filter_data_validation = false;

    $scope.btn_ok = function () {
      if (field_type == 'filter_data') {
        switch ($scope.field_data.type) {
          case 'int':
            if (isNaN(parseFloat($scope.field_data.filter_value)) || !isFinite($scope.field_data.filter_value)) {
              $scope.filter_data_validation = true;
              $scope.filter_data_validation_str = "Number is required!";
              console.log('filter_data_validation', $scope.filter_data_validation_str);
              return false;
            }
            break;
          case 'float':
            if (isNaN(parseFloat($scope.field_data.filter_value)) || !isFinite($scope.field_data.filter_value)) {
              $scope.filter_data_validation = true;
              $scope.filter_data_validation_str = "Number is required!";
              console.log('filter_data_validation', $scope.filter_data_validation_str);
              return false;
            }
            break;
        }
      }

      $uibModalInstance.close();
    };

    $scope.remove_error_message = function (form_field) {
      $scope[form_field] = false;
    };

    $scope.btn_cancel = function () {
      if ($scope.field_data_cancel) {
        angular.copy($scope.field_data_cancel, $scope.field_data);
      }
      $uibModalInstance.dismiss();
    };
  })
  .controller('ReportCtrl', ['$scope', 'Auth', '$state', '$stateParams', '$rootScope', '$uibModal', 'reportAPI', 'Alertify', 'underscore', '$timeout',
    function ($scope, Auth, $state, $stateParams, $rootScope, $uibModal, reportAPI, Alertify, _, $timeout) {
      $scope.screenshot = false;
      $scope.str_reload = '';
      $scope.current_date = new Date();
      $scope.reload = function (id) {
        $scope.str_reload = '?reload=true';
        $scope.chart_id = 0;
        if (typeof(Storage) !== "undefined") {
          sessionStorage.removeItem('report_' + id.toString());
        }
        $scope.chart_id = id;
        $scope.reloadChart();
      };

      $scope.printImage = function (id, type) {
        if ($scope.report.type == 'pie') {
          $scope.report.data_table = $scope.getDataReport();
          $scope.report.header_table = $scope.getHeaderReport();
        }
        $scope.screenshot = 1;
        $timeout(function () {
          var svg = $('#Report' + id + ' .chart-preview .highcharts-container svg')[0];
          if (svg) {
            var xml = new XMLSerializer().serializeToString(svg);
            var data = "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(xml)));
            /*
             var link = document.createElement('a');
             link.download = $scope.report.name + '.svg';
             link.href = data;
             link.click();
             */
            var img = new Image();
            img.setAttribute('src', data);
            img.setAttribute('width', '100%');
            img.setAttribute('class', 'print-image clearfix');
            $('#Report' + id + ' .chart-preview .imagesvg').append(img);
            $('#Report' + id + ' .chart-preview .highcharts-container').css('display', 'none');
          }
          $('#printImage').html($('#Report' + id + ' .chart-preview').clone());

          html2canvas([$('#printImage .chart-preview')[0]], {
            height: 10000,
            allowTaint: true,
            onrendered: function (canvas) {
              if (type == 'pdf') {
                var imgData = canvas.toDataURL("image/jpeg", 1.0);
                var pdf = new jsPDF();

                var width = 190;
                var height = (width / canvas.width) * canvas.height;

                pdf.addImage(imgData, 'JPEG', 10, 10, width, height);
                pdf.save($scope.report.name + ".pdf");
              } else {
                if (window.ActiveXObject || "ActiveXObject" in window) {
                  var html = "<p>Right-click on image below and Save-Picture-As</p>";
                  html += "<img src='" + canvas.toDataURL() + "' alt='" + $scope.report.name + '.png' + " download='" + $scope.report.name + '.png' + "'/>";
                  var tab = window.open();
                  tab.document.write(html);
                } else {
                  var link = document.createElement('a');
                  link.download = $scope.report.name + '.png';
                  link.href = canvas.toDataURL("image/png");//.replace("image/png", "image/octet-stream");
                  link.target = '_blank';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
                }
              }
              if (svg) {
                $('#Report' + id + ' .chart-preview img.print-image').remove();
                $('#Report' + id + ' .chart-preview .highcharts-container').css('display', 'block');
              }
              $('#printImage').html('');
              $scope.screenshot = false;
              $scope.$apply();
            }
          });
        }, 500);
      };

      $scope.$watch('is_preview', function () {
        if (typeof($scope.is_preview) != 'undefined' && parseInt($scope.is_preview) != 0) {
          $scope.chart_id = parseInt($scope.chart_settings.id);
          $scope.reloadChart();
        }
      });

      $scope.set_chart_id = function (id) {
        $scope.chart_id = id;
      };

      $scope.getHeaderReport = function () {
        var header_report = [];
        switch ($scope.report.type) {
          case 'table':
            for (var i in $scope.report.config.fields) {
              header_report.push($scope.report.config.fields[i].label);
            }
            break;
          case 'pie':
            header_report.push($scope.report.config.dimx[0].label);
            header_report.push($scope.report.config.fact[0].label);
            header_report.push($scope.report.config.fact[0].label + ' ratio');
            break;
          case 'column':
          case 'bar':
          case 'area':
          case 'line':
            header_report = angular.copy($scope.report.xAxis.categories);
            header_report.unshift($scope.report.config.dimy[0].label + " \\ " + $scope.report.config.dimx[0].label);
            break;
          case 'group':
            header_report.push($scope.report.config.dimx[0].label);
            header_report.push($scope.report.config.dimy[0].label);
            for (var i in $scope.report.config.fact) {
              header_report.push($scope.report.config.fact[i].label);
            }
            break;
        }

        return header_report;
      };

      $scope.getDataReport = function () {
        switch ($scope.report.type) {
          case 'table':
            return $scope.report.data;
            break;
          case 'pie':
            var data = [];
            var sum = 0;
            for (var i in $scope.report.series[0].data) {
              sum += $scope.report.series[0].data[i].y;
            }

            for (var i in $scope.report.series[0].data) {
              data.push([
                $scope.report.series[0].data[i].name,
                $scope.report.series[0].data[i].y,
                $rootScope.decimalAdjust('round', parseFloat($scope.report.series[0].data[i].y / sum * 100), -2).toString() + " %"
              ]);
            }
            return data;
            break;
          case 'column':
          case 'bar':
          case 'area':
          case 'line':
            var data = [];
            for (var i in $scope.report.series) {
              var row = angular.copy($scope.report.series[i].data);
              row.unshift($scope.report.series[i].name);
              data.push(row);
            }
            return data;
            break;
          case 'group':
            var data = [];
            for (var i in $scope.report.data) {
              for (var j in $scope.report.data[i].dimy) {
                var row = [$scope.report.data[i]._id];
                row.push($scope.report.data[i].dimy[j].name);
                for (var k in $scope.report.config.fact) {
                  row.push($scope.report.data[i].dimy[j][$scope.report.config.fact[k].name + $scope.report.config.fact[k].operator.replace("$", "_")]);
                }
                data.push(row);
              }
            }
            return data;
            break;
        }
      };

      $scope.loadReport = function (resp) {
        try {
          if (resp.type == 'table') {
            var reformat = [];
            for (var i in resp.config.fields) {
              var field = resp.config.fields[i];
              if (field.type == 'datetime') {
                reformat.push(field.name);
              }
            }

            if (reformat.length > 0) {
              for (var i in resp.data) {
                var data = resp.data[i];
                for (var j in data) {
                  if ($rootScope.inArray(j, reformat)) {
                    data[j] = new Date(Date.parse(data[j].substr(0, 10) + "T" + data[j].substr(11, 8) + "Z"));
                  }
                }
              }
            }
          }
          $scope.report = resp;
          $scope.current_page = 1;
          $scope.str_reload = '';
          if ($scope.report.type != 'table') {
            try {
              var label_fact = $scope.report.config.fact[0].label;
            } catch (e) {
              var label_fact = ''
            }
            var config = {
              review: 0,
              noData: 'No data',//'Please wait...',
              loading: true,
              options: {
                chart: {
                  type: $scope.report.type,
                  inverted: $scope.report.inverted == 'True',
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  options3d: {
                    enabled: (resp.depth) ? true : false,
                    alpha: resp.depth ? parseInt(resp.depth) : 0
                  },
                  spacing: [10, 10, 10, 10],
                  style: {
                    fontFamily: 'Arial, Helvetica, sans-serif',
                    fontSize: '15px',
                    fontWeight: 'normal'
                  }
                },
                tooltip: {
                  valueDecimals: 2,
                  style: {
                    padding: 10,
                    fontWeight: 'bold'
                  },
                  valuePrefix: ($scope.report.config.fact && $scope.report.config.fact[0].unit && $scope.report.config.fact[0].unit_option) ? $rootScope.units[$scope.report.config.fact[0].unit][$scope.report.config.fact[0].unit_option].prefix : '',
                  valueSuffix: ($scope.report.config.fact && $scope.report.config.fact[0].unit && $scope.report.config.fact[0].unit_option) ? $rootScope.units[$scope.report.config.fact[0].unit][$scope.report.config.fact[0].unit_option].suffix : ''
                },
                plotOptions: {}
              },
              series: $scope.report.series,
              title: {
                text: ''
              },
              yAxis: {
                title: {
                  text: label_fact
                }
              },
              useHighStocks: false,
              func: function (chart) {
                $scope.chart = chart;
              }
            };
            //Set depth
            if ($rootScope.types[$scope.report.type]) {
              if ($rootScope.types[$scope.report.type]['options'][$scope.report.option] && $rootScope.types[$scope.report.type]['options'][$scope.report.option]['depth'] && resp.depth) {
                $rootScope.types[$scope.report.type]['options'][$scope.report.option]['depth'] = parseInt(resp.depth);
              }
              config['options']['plotOptions'][$scope.report.type] = $rootScope.types[$scope.report.type]['options'][$scope.report.option];
            }
            if ($scope.report.xAxis) {
              config.xAxis = $scope.report.xAxis;
            }
            $scope.chartConfig = config;
            $timeout(function () {
              $scope.chartConfig['review'] = Date.now();
            }, 500);
          }
          //$scope.chart_id = 0;
        } catch (ex) {
          console.log(ex);
          console.log(resp);
        }
      };

      $scope.reloadChart = function () {
        if (typeof($scope.chart_id) != 'undefined' && parseInt($scope.chart_id) != 0) {
          $scope.export_csv_url = report_url + 'report/' + $scope.chart_id + '?format=csv&cid=' + localStorage.getItem("cid").toString() + "&token=" + localStorage.getItem("token").toString();
          var rresp = null;
          if (typeof(Storage) !== "undefined") {
            rresp = sessionStorage.getItem('report_' + $scope.chart_id.toString());
            if (rresp) {
              $timeout(function () {
                $scope.loadReport(JSON.parse(rresp));
                $scope.$apply();
              }, 100);
            }
          }
          if (rresp == null) {
            $scope.chartConfig = {
              noData: 'Please wait...',
              loading: true,
              title: {
                text: ''
              },
              options: {}
            };
            reportAPI.get('report/' + $scope.chart_id + $scope.str_reload).success(function (resp) {
              $scope.loadReport(resp);
              if (typeof(Storage) !== "undefined") {
                sessionStorage.setItem('report_' + resp.id.toString(), JSON.stringify(resp));
              }
            }).error(function (error) {
              //try {
              //  Alertify.error(error.title + ': ' + error.description);
              //} catch (e) {
              //  Alertify.error("Error occurred during connection to server");
              //}
              //$scope.chart_id = 0;
            });
          }
        }
      };

      $scope.$watch('chart_id', function () {
        $scope.reloadChart();
      });
    }])
  .controller('FilterCtrl', ['$scope', 'Auth', '$state', '$stateParams', '$rootScope', '$uibModal', 'underscore', 'reportAPI', 'Alertify', '$filter', '$timeout',
    function ($scope, Auth, $state, $stateParams, $rootScope, $uibModal, _, reportAPI, Alertify, $filter, $timeout) {
      $timeout(function () {
        if ($scope.report && $scope.report.reload && $scope.report.reload != '0') {
          $rootScope.interval_reload = ($rootScope.interval_reload) ? $rootScope.interval_reload : {};

          if (!$rootScope.interval_reload[$scope.report.id]) {
            $rootScope.interval_reload[$scope.report.id] = setInterval(function () {
              if (typeof(Storage) !== "undefined") {
                sessionStorage.removeItem('report_' + $scope.report.id.toString());
              }
              $scope.change_filter();
            }, parseInt($scope.report.reload) * 1000);
          }
        }
      }, 1000);
      $scope.next = function () {
        if ($scope.current_page < Math.ceil($scope.report.total / $scope.report.limit)) {
          $scope.current_page += 1;
          $scope.change_filter(true);
        }
      };
      $scope.prev = function () {
        if ($scope.current_page > 1) {
          $scope.current_page -= 1;
          $scope.change_filter(true);
        }
      };

      $scope.printPDF = function () {
        $scope.pdf_api = ($scope.pdf_api) ? $scope.pdf_api : 'report/' + $scope.report.id + '?format=fulljson';
        reportAPI.get($scope.pdf_api)
          .success(function (resp) {
            //console.log('resp', resp);
            var chart_data = resp.data;
            var chart_fields = resp.config.fields;
            var chart_type = resp.type;
            if (chart_type == 'table') {
              //No reload data
            } else {
              $scope.report.data = resp.data;
            }

            $scope.screenshot = true;
            $timeout(function () {
              $('#printImage').html($('#Report' + $scope.report.id + ' .chart-preview').clone());
              var page_size = $('#printImage').width();
              var pixel2mm = 0.264583333;

              if (chart_type == 'table') {
                var doc = new jsPDF('landscape');
                var doc_width = 297;
                var doc_height = 210;

                var margin_top = 10;
                var margin_bottom = 10;
                var margin_left = 10;
                var border_right = 287;
              } else {
                var doc = new jsPDF();
                var doc_width = 210;
                var doc_height = 297;

                var margin_top = 10;
                var margin_bottom = 10;
                var margin_left = 10;
                var border_right = 200;
              }
              doc.setLineWidth(0.1);
              var x = margin_left;
              var y = margin_top;

              var fields = {};
              var height = 0;

              var dashboard_name = $('#report_dash_board_name').text();
              dashboard_name = (dashboard_name) ? dashboard_name + ': ' : '';
              //print chart name
              doc.setFontSize(16);
              doc.text(x, y, dashboard_name + resp.name);
              y += 40 * pixel2mm;

              //print filter
              doc.setFontSize(11);
              var filter_condition = '';
              $scope.report.config.filters.forEach(function (data) {
                filter_condition += data.label + ": " + ((data.value) ? data.value : 'All') + "  ";
              });
              if (filter_condition != '') {
                doc.text(x, y, filter_condition);
                y += 40 * pixel2mm;
              }

              //print table head
              if (resp.type != 'group') {
                doc.setFontType("bold");
                height = 40 * pixel2mm;
                $('#printImage th').each(function () {
                  doc.text(x, y, $(this).attr("label"));
                  x += ($(this).width() / page_size ) * (doc_width - margin_left);
                });

                doc.line(margin_left - 1, y - height + 3, border_right, y - height + 3);
                doc.line(margin_left - 1, y + 3, border_right, y + 3);
              }

              doc.setFontType("normal");
              doc.setFontSize(9);
              var border_start = y - height + 3;
              var start_sub_table = 0;
              if (chart_type == 'table') {
                var array_x = [];
                x = margin_left;
                array_x.push(x);
                var $first_row = $('#printImage tbody tr:first');
                $first_row.find("td").each(function () {
                  x += ($(this).width() / page_size ) * (doc_width - margin_left);
                  array_x.push(x);
                });

                var start_sub_table = y - 2;
                var y_line = 8.2;

                for (var i in chart_data) {
                  var row = chart_data[i], x_pos = 0;
                  y += y_line;
                  if (y > doc_height - margin_bottom) {
                    //right-boder, left border
                    doc.line(margin_left - 1, start_sub_table - 5, margin_left - 1, y - y_line + 3);
                    doc.line(border_right, start_sub_table - 5, border_right, y - y_line + 3);
                    y = margin_top;
                    start_sub_table = margin_top;
                    doc.addPage();
                    border_start = margin_top - y_line + 3;
                    //first line
                    doc.line(margin_left - 1, margin_top - y_line + 3, border_right, margin_top - y_line + 3);
                  }

                  //console.log('row', row);
                  //console.log('chart_fields', chart_fields);
                  for (var i in chart_fields) {
                    //console.log('chart_fields[i].name', chart_fields[i].name);
                    //console.log('row[chart_fields[i].name]', row[chart_fields[i].name]);
                    if (chart_fields[i].unit) {
                      field_value = $filter('unit')(row[chart_fields[i].name], chart_fields[i].unit, chart_fields[i].unit_option);
                    } else {
                      var field_value = row[chart_fields[i].name];
                    }

                    doc.text(array_x[x_pos], y, field_value.toString());
                    x_pos += 1;
                  }
                  //endline-boder
                  doc.line(margin_left - 1, y + 3, border_right, y + 3);
                }

              } else {
                $('#printImage tbody tr').each(function () {
                  var flag_write = $(this).attr('class');

                  y += $(this).height() * pixel2mm;
                  if (y > doc_height - margin_bottom) {
                    if (resp.type == 'group') {
                      //left-boder
                      doc.line(margin_left - 1, start_sub_table - 6, margin_left - 1, y - $(this).height() * pixel2mm + 3);
                      //right-boder
                      doc.line(border_right, start_sub_table - 6, border_right, y - $(this).height() * pixel2mm + 3);
                    } else {
                      doc.line(margin_left - 1, border_start, margin_left - 1, y - $(this).height() * pixel2mm + 3);
                      doc.line(x, border_start, x, y - $(this).height() * pixel2mm + 3);
                    }

                    y = margin_top;
                    start_sub_table = margin_top;
                    doc.addPage();
                    border_start = margin_top - $(this).height() * pixel2mm + 3;
                    //first line
                    doc.line(margin_left - 1, margin_top - $(this).height() * pixel2mm + 3, x, margin_top - $(this).height() * pixel2mm + 3);
                  }
                  x = margin_left;
                  $(this).find("td").each(function () {
                    var height_to_plush = $(this).width();
                    if (flag_write.indexOf('row-name') >= 0) {
                      height_to_plush = 20;
                      doc.setFontSize(14);
                      doc.text(x, y, $(this).text().trim());
                      doc.setFontSize(11);
                      start_sub_table = y + $(this).height() * pixel2mm;
                    } else if (flag_write.indexOf('row-head') >= 0) {
                      doc.setFontType("bold");
                      doc.text(x, y, $(this).text().trim());
                      doc.setFontType("normal");
                    } else if (flag_write.indexOf('row-foot') >= 0) {
                      doc.setFontType("bold");
                      doc.text(x, y, $(this).text().trim());
                      doc.setFontType("normal");
                    } else {
                      doc.text(x, y, $(this).html());
                    }

                    x += (height_to_plush / page_size ) * 200;
                  })

                  //endline-boder
                  doc.line(margin_left - 1, y + 3, border_right, y + 3);

                  if (flag_write.indexOf('row-foot') >= 0) {
                    //left-boder
                    doc.line(margin_left - 1, start_sub_table - 6, margin_left - 1, y + 3);
                    //right-boder
                    doc.line(border_right, start_sub_table - 6, border_right, y + 3);
                  }
                });
              }


              if (resp.type != 'group') {
                //left-boder
                doc.line(margin_left - 1, border_start, margin_left - 1, y + 3);
                //right-boder
                doc.line(border_right, border_start, border_right, y + 3);
              }

              doc.save($scope.report.name + ".pdf");
              $scope.change_filter();
              $('#printImage').html('');
              $scope.screenshot = false;
              $scope.$apply();
            }, 500);
          });
      };

      $scope.printCSV = function () {
        $scope.pdf_api = ($scope.pdf_api) ? $scope.pdf_api : 'report/' + $scope.report.id + '?format=fulljson';
        reportAPI.get($scope.pdf_api)
          .success(function (resp) {
            $scope.report.data = resp.data;

            $scope.screenshot = true;
            setTimeout(function () {
              $('#printImage').html($('#Report' + $scope.report.id + ' .chart-preview').clone());

              var str_data = '';

              $('#printImage th').each(function () {
                str_data += '"' + $(this).attr("label") + '"' + ',';
              });

              str_data = str_data + "\r\n";

              $('#printImage tbody tr').each(function () {
                $(this).find("td").each(function () {
                  if ($(this).html().indexOf(",") >= 0) {
                    str_data += '"' + $(this).html() + '"' + ',';
                  } else {
                    str_data += $(this).html() + ',';
                  }

                });
                str_data = str_data + "\r\n";
              });

              var encodedUri = 'data:application/csv;charset=utf-8,' + encodeURIComponent(str_data);
              var link = document.createElement("a");
              link.setAttribute("href", encodedUri);
              link.setAttribute("target", '_blank');
              link.setAttribute("download", $scope.report.name + ".csv");
              link.click();

              $scope.change_filter();
              $('#printImage').html('');
              $scope.screenshot = false;
              $scope.$apply();
            }, 500);
          });
      };

      $scope.change_filter = function (is_paging) {
        var query_filter = '';
        for (var i in $scope.report.config.filters) {

          if (is_paging !== true) {
            $scope.current_page = 1;
          }

          if ($scope.report.config.filters[i].value != 'All' && $scope.report.config.filters[i].value != undefined) {
            if ($scope.report.config.filters[i].type == 'date' || $scope.report.config.filters[i].type == 'datetime') {
              var date_string = $filter('date')($scope.report.config.filters[i].value, "yyyy-MM-dd HH:mm:ss");
              query_filter = query_filter + $scope.report.config.filters[i].name + "=" + date_string + '&';
            } else {
              query_filter = query_filter + $scope.report.config.filters[i].name + "=" + $scope.report.config.filters[i].value + '&';
            }
          }
        }
        if ($scope.report.type == "table") {
          query_filter += 'page=' + $scope.current_page + '&';
        }
        query_filter = query_filter.substr(0, query_filter.length - 1);
        $scope.export_csv_url = report_url + 'report/' + $scope.report.id + '?format=csv&' + query_filter + ( (query_filter) ? "&" : "" ) + "cid=" + localStorage.getItem("cid").toString() + "&token=" + localStorage.getItem("token").toString() + "&time=" + localStorage.getItem("time").toString();
        $scope.pdf_api = 'report/' + $scope.report.id + '?html=true&' + query_filter;
        reportAPI.get('report/' + $scope.report.id + "?" + query_filter)
          .success(function (resp) {
            $scope.report.data = resp.data;
            if (resp.data) {
              $scope.report.data = resp.data;
            }
            if (resp.total) {
              $scope.report.total = resp.total;
            }
            if (resp.series) {
              $scope.report.series = resp.series;
            }
            if (resp.xAxis) {
              $scope.report.xAxis = resp.xAxis;
            }
            if ($scope.chartConfig) {
              $scope.chartConfig.series = $scope.report.series;
            }
          });
      };
    }
  ])
  .controller('ViewChartCtrl', ['$scope', 'Auth', '$state', '$stateParams', '$rootScope', 'underscore', '$uibModal', 'reportAPI', 'Alertify', '$location', '$timeout',
    function ($scope, Auth, $state, $stateParams, $rootScope, _, $uibModal, reportAPI, Alertify, $location, $timeout) {
      $rootScope.clear_interval_reload();
      $rootScope.init_default_dashboad_id();
      $scope._type = $stateParams.type;
      $scope._id = $stateParams.id;
      $scope.chart_id = $stateParams.id;
      //$scope.$apply();
    }])
  .controller('SortTableCtrl', ['$scope', 'Auth', '$state', '$stateParams', '$rootScope', 'underscore', '$uibModal', 'reportAPI', 'Alertify', '$location', '$timeout',
    function ($scope, Auth, $state, $stateParams, $rootScope, _, $uibModal, reportAPI, Alertify, $location, $timeout) {
      $scope.tableSortType = '';
      $scope.tableSortReverse = true;
      $scope.tableSort = function (sortType) {
        if ($scope.tableSortReverse == true && $scope.tableSortType != '') {
          $scope.tableSortType = '';
          return;
        }
        $scope.tableSortType = sortType;
        $scope.tableSortReverse = !$scope.tableSortReverse;
      };
    }])
;
