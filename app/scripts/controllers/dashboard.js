'use strict';
$ = jQuery;
var marker = null;
var map = null;
/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl', function ($scope, reportAPI, Auth, $stateParams, $rootScope, $location, $state, Alertify, $uibModal, $timeout) {
    $rootScope.clear_interval_reload();

    $rootScope.current_dashboard_id = $stateParams.id;
    $scope.gridsterOpts = {
      columns: 24, // the width of the grid, in columns
      pushing: true, // whether to push other items out of the way on move or resize
      floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
      swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
      width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
      colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
      rowHeight: "match", // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
      margins: [10, 10], // the pixel distance between each widget
      outerMargin: true, // whether margins apply to outer edges of the grid
      isMobile: true, // stacks the grid items if true
      mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
      mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
      minColumns: 1, // the minimum columns the grid must have
      minRows: 30, // the minimum height of the grid, in rows
      maxRows: 100,
      defaultSizeX: 4, // the default width of a gridster item, if not specifed
      defaultSizeY: 4, // the default height of a gridster item, if not specified
      minSizeX: 4, // minimum column width of an item
      maxSizeX: null, // maximum column width of an item
      minSizeY: 4, // minumum row height of an item
      maxSizeY: null, // maximum row height of an item
      resizable: {
        enabled: true,
        handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
        start: function (event, $element, widget) {
        }, // optional callback fired when resize is started,
        resize: function (event, $element, widget) {
        }, // optional callback fired when item is resized,
        stop: function (event, $element, widget) {
        } // optional callback fired when item is finished resizing
      },
      draggable: {
        enabled: true, // whether dragging items is supported
        handle: '.my-class', // optional selector for resize handle
        start: function (event, $element, widget) {
        }, // optional callback fired when drag is started,
        drag: function (event, $element, widget) {
        }, // optional callback fired when item is moved,
        stop: function (event, $element, widget) {
        } // optional callback fired when item is finished dragging
      }
    };
    $scope.update = function () {
      if (typeof(Storage) !== "undefined") {
        sessionStorage.clear();
      }
      if ($stateParams.id) {
        for (var i in $scope.dashboard.charts) {
          delete $scope.dashboard.charts[i]['$$hashKey']
        }
        reportAPI.put('dashboard/' + $stateParams.id, $scope.dashboard).error(function (resp) {
          if (resp && resp.title) {
            $rootScope.show_msg(resp.title)
          }
        }).success(function (data) {
          $scope.dashboard = data
        });
      }
    };
    $scope.delete = function () {
      if (typeof(Storage) !== "undefined") {
        sessionStorage.clear();
      }
      if ($stateParams.id) {
        reportAPI.delete('dashboard/' + $stateParams.id).success(function (data) {
          $scope.dashboard = data;
          $rootScope.init_list_dashboard();
          Alertify.success('All charts deleted!')
        }).error(function (resp) {
          if (resp && resp.title) {
            Alertify.success(resp.title);
          }
        });
      }
    };
    $scope.deleteChart = function (id) {
      reportAPI.delete('chart/' + id)
        .success(function (resp) {
          var charts = [];
          for (var i in $scope.dashboard.charts) {
            if ($scope.dashboard.charts[i]['chart_id'] != id) {
              charts.push($scope.dashboard.charts[i]);
            }
          }
          $scope.dashboard.charts = charts;
          Alertify.success('Chart deleted!')
        }).error(function (error) {
          if (error && error.title) {
            $rootScope.show_msg(error.title)
          }
        });
    };
    $scope.set_enable_change = function () {
      $scope.dashboard.enable_change = !$scope.dashboard.enable_change;
      $scope.gridsterOpts.draggable.enabled = $scope.dashboard.enable_change;
      $scope.gridsterOpts.resizable.enabled = $scope.dashboard.enable_change;
    };
    $scope.clone_dashboad = function () {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/popup/clone_dashboad.html',
        controller: 'CloneDashboadCtrl',
        size: 'lg',//sm
        resolve: {
          dashboad_id: function () {
            return $scope.dashboard.id
          }
        }
      });
      $rootScope.popups.push(modalInstance);
    };
    $scope.loadDashboard = function (data) {
      $rootScope.init_list_dashboard();
      $scope.dashboard = data;
      $stateParams.id = $scope.dashboard.id;
      //set cookie default_dashboard_id
      $rootScope.default_dashboard_id = $scope.dashboard.id;
      $.cookie("default_dashboard_id", $rootScope.default_dashboard_id);
      if (!"enable_change" in $scope.dashboard) {
        $scope.dashboard.enable_change = true;
      } else {
        if ($scope.dashboard.enable_change == "False" || $scope.dashboard.enable_change == "false") {
          $scope.dashboard.enable_change = false;
        }
      }
      $scope.gridsterOpts.draggable.enabled = $scope.dashboard.enable_change;
      $scope.gridsterOpts.resizable.enabled = $scope.dashboard.enable_change;
    };
    if ($state.current.name == 'company') {
      var data = {
        cid: $stateParams.cid,
        token: $stateParams.token,
        time: $stateParams.time
      };
      Auth.createSession(data);
      $timeout(function(){
        reportAPI.get('company/' + $stateParams.cid).success(function (data) {
          $scope.loadDashboard(data);
        });
      },1000);
    } else {
      if ($state.current.name == 'dashboardIframe') {
        $timeout(function () {
          $scope.dashboard.enable_change = false;
          $scope.gridsterOpts.draggable.enabled = false;
          $scope.gridsterOpts.resizable.enabled = false;
        }, 300);
      }
      reportAPI.get('dashboard/' + $stateParams.id).success(function (data) {
        $scope.loadDashboard(data);
      });
    }
  })
  .controller('AddDashboadCtrl', function ($scope, $rootScope, $uibModalInstance, Alertify, reportAPI, $location) {
    $scope.dashboad_name = '';
    $scope.btn_ok = function () {
      if ($scope.dashboad_name == '') {
        Alertify.error("Please input name for new dashboard");
        return;
      }

      var data = {
        name: $scope.dashboad_name,
        company_id: $.cookie("cid"),
        enable_change: true,
        charts: ''
      };

      reportAPI.post('dashboard', data).success(function (resp) {
        Alertify.success("Create dashboad successful!");
        $rootScope.init_list_dashboard();
        $location.path('dashboard/' + resp.id);
        $uibModalInstance.close();
      });
    };

    $scope.btn_cancel = function () {
      $uibModalInstance.dismiss();
    };
  })
  .controller('CloneDashboadCtrl', function ($scope, $rootScope, $uibModalInstance, Alertify, reportAPI, dashboad_id, $location) {
    $scope.list_dashboards = [];
    reportAPI.get('dashboards').success(function (resp) {
      resp.unshift({
        company_id: "",
        name: "Please choose dashboad",
        id: "0"
      });
      $scope.list_dashboards = resp;
    }).error(function (error) {
      try {
        Alertify.error(error.title + ': ' + error.description);
      } catch (e) {
        Alertify.error("Error occurred during connection to server");
      }
    });
    $scope.choosen_dashboard_id = "0";
    $scope.btn_ok = function () {
      if ($scope.choosen_dashboard_id == 0) {
        Alertify.error("Please choose dashboad before copy");
        return;
      }
      reportAPI.get('clone/' + $scope.choosen_dashboard_id.toString() + "/" + dashboad_id.toString()).success(function (resp) {
        Alertify.success("Copy dashboad successful!");
        $location.path('/dashboard/' + dashboad_id.toString());
        $uibModalInstance.close();
      }).error(function (error) {
        try {
          Alertify.error(error.title + ': ' + error.description);
        } catch (e) {
          Alertify.error("Error occurred during connection to server");
        }
      });
    };

    $scope.btn_cancel = function () {
      $uibModalInstance.dismiss();
    };
  });
