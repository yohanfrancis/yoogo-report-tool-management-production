'use strict';
$ = jQuery;

angular.module('yapp')
  .directive('ngConfirm', function (Alertify) {
    return {
      priority: 1,
      terminal: true,
      link: function (scope, element, attr) {
        var msg = attr.ngConfirm || "Are you sure?";
        var clickAction = attr.ngClick;
        element.bind('click', function () {
          Alertify.confirm(msg).then(
            function onOk() {
              scope.$eval(clickAction);
            },
            function oncancel() {
            }
          );
        });
      }
    };
  })
;
