'use strict';
$ = jQuery;
var report_url = 'https://report-api.yoogoshare.co.nz/';
if (!$.cookie("company")) {
  $.cookie("company", 'keaz');
}
angular.module('service', [])
  .factory('Auth', function ($rootScope) {
    return {
      destroyAll: function () {
        localStorage.removeItem('cid');
        localStorage.removeItem('token');
        localStorage.removeItem('time');
        //$.removeCookie('cid');
        //$.removeCookie('token');
        //$.removeCookie('time');
      },
      createSession: function (data) {

        localStorage.setItem("cid", data.cid);
        localStorage.setItem("token", data.token);
        localStorage.setItem("time", data.time);
        //$.cookie("cid", data.cid);
        //$.cookie("token", data.token);
        //$.cookie("time", data.time);
      },
      getSession: function () {
        if (localStorage.getItem("token")) {
          return {
            'cid': localStorage.getItem("cid"),
            'token': localStorage.getItem("token"),
            'time': localStorage.getItem("time")
          }
        }
        return null;
      }
    }
  })
  .factory('Report', ["$http", "$location", "$rootScope", "Auth", "$timeout","Alertify", function ($http, $location, $rootScope, Auth, $timeout, Alertify) {
    return {
      get: function (option) {
        var validate_response = function (data) {
          $rootScope.load = false;
          if (data && data.title) {
            try {
              Alertify.error(data.title + ': ' + data.description);
            } catch (e) {
              Alertify.error("Error occurred during connection to server");
            }
            return false;
          }
        };
        try {
          if (localStorage.getItem("cid") && localStorage.getItem("token")) {
            option.url = option.url + ((option.url.indexOf("?") >= 0) ? "&" : "?") + "cid=" + localStorage.getItem("cid").toString() + "&token=" + localStorage.getItem("token").toString() +  "&time=" + localStorage.getItem("time").toString();
          }
        } catch (e) {
          console.log(e.toString());
        }
        $rootScope.load = true;
        return $http(option).success(validate_response).error(validate_response);
      }
    }
  }])
  .factory('reportAPI', ["Report", '$location', '$rootScope', '$timeout', '$http', function (Report, $location, $rootScope, $timeout, $http) {
    var reportAPI = {};
    reportAPI.get = function (_url) {
      var URL = report_url + _url;
      return Report.get({
        method: 'GET',
        url: URL
      });
    };
    reportAPI.post = function (_url, _data) {
      var data = JSON.stringify(_data),
        url = report_url + _url;
      return Report.get({
        method: 'POST',
        url: url,
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      });
    };
    reportAPI.delete = function (_url) {
      var url = report_url + _url;
      return Report.get({
        method: 'DELETE',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      });
    };
    reportAPI.put = function (_url, _data) {
      var data = JSON.stringify(_data),
        url = report_url + _url;
      return Report.get({
        method: 'PUT',
        url: url,
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      });
    };
    return reportAPI;
  }]);
