'use strict';

$ = jQuery;
angular.module('yapp').run(function ($rootScope, reportAPI, $location, Auth, $timeout, $state, $stateParams, Alertify, $uibModal) {
  $rootScope.clear_interval_reload = function () {
    for(var id in $rootScope.interval_reload){
      clearInterval($rootScope.interval_reload[id]);
      delete $rootScope.interval_reload[id];
    }
  };

  $rootScope.init_list_dashboard = function (is_redirect_default) {
    reportAPI.get('dashboards').success(function (data) {
      if(data){
        $rootScope.list_dashboard_menus = data;
        if (is_redirect_default) {
          $rootScope.redirect_default_dashboard();
        }
      }
    });
  };

  $rootScope.redirect_default_dashboard = function () {
    for (var i in $rootScope.list_dashboard_menus) {
      if ($rootScope.list_dashboard_menus[i].default == true) {
        $location.path("dashboard/" + $rootScope.list_dashboard_menus[i].id.toString());
      }
    }
  };
  $rootScope.decimalAdjust = function (type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }
  $rootScope.delete_dashboard_menu = function (id) {
    reportAPI.delete("/dashboard/" + id.toString() + "/delete").success(function () {
      $rootScope.init_list_dashboard(true);
      Alertify.success("Dashboad deleted!");
    });
  };

  $rootScope.set_default_dashboard_menu = function (id) {
    for (var i in $rootScope.list_dashboard_menus) {
      if ($rootScope.list_dashboard_menus[i].id == id) {
        $rootScope.list_dashboard_menus[i].default = true;

        reportAPI.put("dashboard/" + id.toString(), $rootScope.list_dashboard_menus[i]).success(function () {
          $rootScope.init_list_dashboard();
          Alertify.success("Set default dashboad successful!");
        });
      }
    }
  };

  $rootScope.add_dashboard_menu = function () {
    var modalInstance = $uibModal.open({
      animation: $rootScope.animationsEnabled,
      templateUrl: 'views/popup/add_dashboard.html',
      controller: 'AddDashboadCtrl',
      size: 'lg',//sm
    });
    $rootScope.popups.push(modalInstance);
  };

  $rootScope.init_list_dashboard();

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    sessionStorage.clear()
    for (var i in $rootScope.popups) {
      $rootScope.popups[i].close();
    }
    $rootScope.popups = [];
  });
  $rootScope.types = {
    'column': {
      'name': 'Column',
      'value': 'column',
      'limit': {
        'fact': 1,
        'dimx': 1,
        'dimy': 1
      },
      'options': {
        'none': {},
        'nomal': {
          stacking: 'normal',
        },
        percent: {
          stacking: 'percent'
        },
        grouping: {
          grouping: false,
          shadow: false,
          borderWidth: 0
        }
      }
    },
    'bar': {
      'name': 'Bar',
      'value': 'bar',
      'options': {
        'none': {},
        'nomal': {
          stacking: 'normal'
        },
        percent: {
          stacking: 'percent'
        },
        grouping: {
          grouping: false,
          shadow: false,
          borderWidth: 0
        }
      },
      'limit': {
        'fact': 1,
        'dimx': 1,
        'dimy': 1
      },
    },
    'area': {
      'name': 'Area',
      'value': 'area',
      'limit': {
        'fact': 1,
        'dimx': 1,
        'dimy': 1
      },
      'options': {
        'none': {},
        'normal': {
          stacking: 'normal',
          lineColor: '#666666',
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: '#666666'
          }
        },
        'percent': {
          stacking: 'percent',
          lineColor: '#ffffff',
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: '#ffffff'
          }
        },
        'vertical': {
          inverted: true,
          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          }
        }
      }
    },
    'table': {
      'name': 'Table',
      'value': 'table',
      'options': {
        'none': {}
      }
    },
    'group': {
      'name': 'Group',
      'value': 'Group',
      'options': {
        'none': {}
      },
      'limit': {
        'fact': 100,
        'dimx': 1,
        'dimy': 1
      }
    },
    'line': {
      'limit': {
        'fact': 1,
        'dimx': 1,
        'dimy': 1
      },
      'name': 'Line chart',
      'value': 'line',
      'options': {
        'none': {},
        'show_value': {
          dataLabels: {
            enabled: true
          },
          spline: {
            marker: {
              enabled: true
            }
          }
        }
      }
    },
    'pie': {
      'name': 'Pie Chart',
      'value': 'pie',
      'limit': {
        'fact': 1,
        'dimx': 1,
        'dimy': 0
      },
      'options': {
        'none': {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          tooltip: {
            style: {
              padding: 10,
              fontWeight: 'bold'
            },
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          depth: 45,

        },
        'donut_none': {
          allowPointSelect: true,
          innerSize: 80,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          tooltip: {
            style: {
              padding: 10,
              fontWeight: 'bold'
            },
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          depth: 45,

        },
        'label': {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true
          },
          tooltip: {
            style: {
              padding: 10,
              fontWeight: 'bold'
            },
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          depth: 45,
        },
        'legend': {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true,
          tooltip: {
            style: {
              padding: 10,
              fontWeight: 'bold'
            },
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          depth: 45,
        },
        'donut_legend': {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          innerSize: 80,
          showInLegend: true,
          tooltip: {
            style: {
              padding: 10,
              fontWeight: 'bold'
            },
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          depth: 45,
        },
        'donut_label': {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true
          },
          innerSize: 80,
          showInLegend: false,
          tooltip: {
            style: {
              padding: 10,
              fontWeight: 'bold'
            },
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          depth: 45,
        }
      }
    }
  };
  $rootScope.units = {
    distance: {
      none: {
        label: 'None',
        prefix: '',
        suffix: ''
      },
      km: {
        label: 'KM',
        prefix: '',
        suffix: 'km'
      },
      mile: {
        label: 'MILE',
        prefix: '',
        suffix: 'mile'
      },
      m: {
        label: 'M',
        prefix: '',
        suffix: 'm'
      }
    },
    last_time: {
      none: {
        label: 'None',
        prefix: '',
        suffix: ''
      },
      natural: {
        label: 'Natural Time',
        prefix: '',
        suffix: ''
      },
    },
    duration: {
      none: {
        label: 'None',
        prefix: '',
        suffix: ''
      },
      natural: {
        label: 'Natural Time',
        prefix: '',
        suffix: ''
      },
    },
    time: {
      none: {
        label: 'None',
        prefix: '',
        suffix: ''
      },
      hour: {
        label: 'Hour',
        prefix: '',
        suffix: 'hour'
      },
      minute: {
        label: 'Minute',
        prefix: '',
        suffix: 'minute'
      }
    },
    datetime: {
      'timeformat1': {
        label: 'yyyy-MM-dd HH:mm:ss',
        prefix: '',
        suffix: ''
      },
      'timeformat2': {
        label: 'MM/dd/yyyy @ h:mm a',
        prefix: '',
        suffix: ''
      }
    },
    currency: {
      none: {
        label: 'None',
        prefix: '',
        suffix: ''
      },
      usd: {
        label: 'USD',
        prefix: '$',
        suffix: ' USD'
      },
      aud: {
        label: 'AUD',
        prefix: '$',
        suffix: ' AUD'
      },
      jpy: {
        label: 'JPY',
        prefix: '¥',
        suffix: ''
      },
      gbp: {
        label: 'GBP',
        prefix: '£',
        suffix: ' GBP'
      },
      eur: {
        label: 'EUR',
        prefix: '€',
        suffix: ' EUR'
      },
      ndz: {
        label: 'NDZ',
        prefix: '$',
        suffix: ' NDZ'
      }
    }
  };
  $rootScope.operators = {
    'int': [
      {
        'name': "Count",
        'value': ""
      }, {
        'name': "Sum",
        'value': "$sum"
      },
      {
        'name': "Avg",
        'value': "$avg"
      },
      {
        'name': "Min",
        'value': "$min"
      },
      {
        'name': "Max",
        'value': "$max"
      }

    ],
    'float': [
      {
        'name': "Count",
        'value': ""
      }, {
        'name': "Sum",
        'value': "$sum"
      },
      {
        'name': "Avg",
        'value': "$avg"
      },
      {
        'name': "Min",
        'value': "$min"
      },
      {
        'name': "Max",
        'value': "$max"
      }
    ],
    'string': [
      {
        'name': "Count",
        'value': ""
      }
    ],
    'date': [
      {
        'name': "Count",
        'value': ""
      }
    ]
  };
  $rootScope.orders = [
    {
      'name': 'Random',
      'value': 0
    },
    {
      'name': 'Ascending',
      'value': 1
    },
    {
      'name': 'Descending',
      'value': -1
    }
  ];
  $rootScope.conditions = {
    string: [
      {
        'name': '=',
        'value': ''
      },
      {
        'name': 'IS NOT',
        'value': '$ne'
      }
    ],
    int: [
      {
        'name': '>',
        'value': '$gt'
      },
      {
        'name': '=',
        'value': ''
      },
      {
        'name': 'IS NOT',
        'value': '$ne'
      },
      {
        'name': '<',
        'value': '$lt'
      },
      {
        'name': '>=',
        'value': '$gte'
      },
      {
        'name': '<=',
        'value': '$lte'
      }
    ],
    float: [
      {
        'name': '>',
        'value': '$gt'
      },
      {
        'name': '=',
        'value': ''
      },
      {
        'name': 'IS NOT',
        'value': '$ne'
      },
      {
        'name': '<',
        'value': '$lt'
      },
      {
        'name': '>=',
        'value': '$gte'
      },
      {
        'name': '<=',
        'value': '$lte'
      }
    ],
    datetime: [
      {
        'name': '>',
        'value': '$gt'
      },
      {
        'name': '=',
        'value': ''
      },
      {
        'name': 'IS NOT',
        'value': '$ne'
      },
      {
        'name': '<',
        'value': '$lt'
      },
      {
        'name': '>=',
        'value': '$gte'
      },
      {
        'name': '<=',
        'value': '$lte'
      }
    ]
  };
  $rootScope.auto_reload_times = [
    {
      name: 'None',
      value: '0'
    },
    {
      name: '5s',
      value: '5'
    },
    {
      name: '10s',
      value: '10'
    },
    {
      name: '30s',
      value: '30'
    },
    {
      name: '1 minutes',
      value: '60'
    },
    {
      name: '2 minutes',
      value: '120'
    },
    {
      name: '3 minutes',
      value: '180'
    },
    {
      name: '4 minutes',
      value: '240'
    },
    {
      name: '5 minutes',
      value: '300'
    }
  ];
  $rootScope.init_field_data = function (field_data, field_type) {
    if (field_data.unit) {
      field_data.unit_option = (field_data.unit_option) ? field_data.unit_option : $rootScope.get_first_key($rootScope.units[field_data.unit]);
    }
    switch (field_type) {
      case 'fields':
        field_data.order = (field_data.order) ? field_data.order : 0;
        break;
      case 'filter_data':
        field_data.filter_value = (field_data.filter_value) ? field_data.filter_value : '';
      case 'filters':
        field_data.condition = (field_data.condition) ? field_data.condition : $rootScope.conditions[field_data.type][0].value;
        break;
      case 'dimx':
        field_data.order = (field_data.order) ? field_data.order : 0;
        break;
      case 'dimy':
        field_data.order = (field_data.order) ? field_data.order : 0;
        break;
      case 'fact':
        field_data.order = (field_data.order) ? field_data.order : 0;
        field_data.operator = (field_data.operator) ? field_data.operator : '';
        break;
    }

    return field_data;
  }


  $rootScope.validation = function () {
    var validateForm = $("form.validate");
    if (validateForm.length > 0) {
      validateForm.validationEngine({
        relative: true
      });
    }
  };

  $rootScope.get_template_chart = function (type) {
    return "views/chart/chart_" + type + ".html";
  };
  // Pagination list
  $rootScope.make_chart_table = function (data, entryLimit) {
    // Pagination
    $rootScope.setPage = function (pageNo) {
      $rootScope.currentPage = pageNo;
    };
    $rootScope.filter = function () {
      $rootScope.currentPage = 1;
      $timeout(function () {
        $rootScope.filteredItems = ($rootScope.filtered) ? $rootScope.filtered.length : 0;
      }, 10);
    };
    $rootScope.sort_by = function (predicate) {
      $rootScope.predicate = predicate;
      $rootScope.reverse = !$rootScope.reverse;
    };
    var response = data;
    if ($rootScope.listing_var) {
      response = response[$rootScope.listing_var];
      $rootScope.listing_var = null;
    }
    $rootScope.list = response;
    if ($rootScope.list) {
      if (entryLimit && typeof entryLimit !== 'undefined') {
        $rootScope.entryLimit = entryLimit;
      } else {
        $rootScope.entryLimit = 10; //max no of items to display in a page
      }
      $rootScope.currentPage = 1; //current page
      if ($rootScope.list) {
        $rootScope.filteredItems = $rootScope.list.length; //Initially for no filter
      }
      $rootScope.totalItems = $rootScope.list.length;
    }
  };

  $rootScope.reformat_option_filter = function (data) {
    for (var i in data) {
      data[i].options.unshift('All');
    }
    return data;
  };
  $rootScope.inArray = function (value, array) {
    return array.indexOf(value) !== -1;
  };

  $rootScope.get_first_key = function (obj) {
    for (var firstKey in obj) break;
    return firstKey;
  };
  $rootScope.minute_to_natural_time = function (t) {
    var str = '';
    var m = t % 60;
    str = m.toString() + ((m > 1) ? ' minutes ' : ' minute ') + str;
    t = Math.floor(t / 60);
    if (t < 1) {
      return str;
    }
    var h = t % 24;
    str = h.toString() + ((h > 1) ? ' hours ' : ' hour ') + str;
    var d = Math.floor(t / 24);
    if (d < 1) {
      return str;
    }
    str = d.toString() + ((d > 1) ? ' days ' : ' day ') + str;
    return str;
  }

  $rootScope.init_default_dashboad_id = function () {
    if (!$rootScope.default_dashboard_id) {
      $rootScope.default_dashboard_id = $.cookie("default_dashboard_id");
    }
    return $rootScope.default_dashboard_id;
  };

  $rootScope.checkActiveMenu = function (state_name, type) {
    if ($state.current.name == state_name) {
      for (var i in type) {
        if ($stateParams[i] != type[i]) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  };

  $rootScope.set_default_dashboard = function (id_dashboad) {

  };
});


