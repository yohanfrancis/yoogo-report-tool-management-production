NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "yapp",
      "shortName": "yapp",
      "type": "overview",
      "moduleName": "yapp",
      "shortDescription": "yapp",
      "keywords": "api application main module overview yapp"
    },
    {
      "section": "api",
      "id": "yapp.controller:MainCtrl",
      "shortName": "MainCtrl",
      "type": "function",
      "moduleName": "yapp",
      "shortDescription": "MainCtrl",
      "keywords": "api controller function mainctrl yapp"
    },
    {
      "section": "api",
      "id": "yapp.controller:MainCtrl",
      "shortName": "MainCtrl",
      "type": "function",
      "moduleName": "yapp",
      "shortDescription": "MainCtrl",
      "keywords": "api controller function mainctrl yapp"
    }
  ],
  "apis": {
    "api": true
  },
  "__file": "_FAKE_DEST_/js/docs-setup.js",
  "__options": {
    "startPage": "/api",
    "scripts": [
      "js/angular.min.js",
      "js/angular-animate.min.js",
      "js/marked.js"
    ],
    "styles": [],
    "title": "API Documentation",
    "html5Mode": true,
    "editExample": true,
    "navTemplate": false,
    "navContent": "",
    "navTemplateData": {},
    "loadDefaults": {
      "angular": true,
      "angularAnimate": true,
      "marked": true
    }
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "js/angular.min.js",
    "js/angular-animate.min.js",
    "js/marked.js"
  ]
};